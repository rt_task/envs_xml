package main.java.com.vpavlova;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

/**
 * Программа, которая по имеющемуся сервису из гита находит все используемые переменные
 * выводит информацию в консоль в xml виде
 * @author Victoria Pavlova
 * @version 1.0
 */
public class Main {

    /**
     * Экземпляр логгера для записи событий и сообщений в журнал.
     * @see Logger
     */
    private static final Logger logger = Logger.getLogger(Main.class.getName());

    public static void main(String[] args) {

        if (args.length == 0) {
            logger.info("Необходимо указать путь к проекту сервиса.");
            return;
        }

        /**
         * Поле для хранения пути, который был передан программе через аргументы командной строки,
         */
        String mainPath = args[0];
        Map<String, String> variables = new HashMap<>();

        ReadFiles.readAppFile(mainPath + "/application.yml", variables);
        ReadFiles.readGradleFile(mainPath + "/build.gradle", variables);
        ReadFiles.readPropertiesFile(mainPath + "/gradle.properties", variables);

        ReadFiles.replaceVariableReferences(variables);
        ReadFiles.printXml(ReadFiles.replaceVariableReferences(variables));
    }
}
