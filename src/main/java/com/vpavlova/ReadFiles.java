package main.java.com.vpavlova;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Класс читает файлы application.yml, build.gradle и gradle.properties.
 * mainPath получаем в качестве параметра командной строки - путь к папке с файлами.
 */
class ReadFiles {

    private static final Logger logger = Logger.getLogger(ReadFiles.class.getName());

    /**
     * Метод, который читает файл application.yml по указанному пути
     * и извлекает переменные с их значениями по шаблону "${variable:defaultValue}".
     * Значения переменных добавляются в указанную карту.
     *
     * @param filePath  Путь к файлу application.yml
     * @param variables Карта, в которую будут добавлены извлеченные переменные и их значения.
     *                  Если переменная с таким именем уже присутствует в карте, она будет перезаписана.
     */
    public static void readAppFile(String filePath, Map<String, String> variables) {
        Pattern pattern = Pattern.compile("\\$\\{([^:]+):([^\\}]*)\\}");
        try (BufferedReader reader = new BufferedReader(new FileReader(filePath))) {
            String line;
            while ((line = reader.readLine()) != null) {
                Matcher matcher = pattern.matcher(line);

                while (matcher.find()) {
                    String variable = matcher.group(1);
                    String defaultValue = matcher.group(2);
                    variables.put(variable, defaultValue);
                }
            }
        } catch (IOException e) {
            logger.info("Ошибка при чтении файла " + filePath + ": " + e.getMessage());
        }
    }

    /**
     * Метод, который считывает файл build.gradle и извлекает переменные и их значения.
     *
     * @param filePath  Путь к файлу build.gradle
     * @param variables Карта с переменными, в которую будут добавлены извлеченные переменные и их значения.
     */
    public static void readGradleFile(String filePath, Map<String, String> variables) {
        Pattern pattern = Pattern.compile("\"(.+?)\"\\s*:\\s*\"(.+?)\"");
        try (BufferedReader reader = new BufferedReader(new FileReader(filePath))) {
            String line;
            boolean insideRunBlock = false;

            while ((line = reader.readLine()) != null) {
                if (line.contains("run {")) {
                    insideRunBlock = true;
                } else if (line.contains("}")) {
                    insideRunBlock = false;
                } else if (insideRunBlock) {
                    Matcher matcher = pattern.matcher(line);
                    while (matcher.find()) {
                        String variable = matcher.group(1);
                        String defaultValue = matcher.group(2);

                        variables.put(variable, defaultValue);
                    }
                }
            }
        } catch (IOException e) {
            logger.info("Ошибка при чтении файла " + filePath + ": " + e.getMessage());
        }
    }

    /**
     * Метод, который считывает файл gradle.properties и извлекает переменные и их значения.
     *
     * @param filePath  путь к файлу gradle.properties
     * @param variables Карта, в которую будут сохранены переменные и их значения
     */
    public static void readPropertiesFile(String filePath, Map<String, String> variables) {
        try (BufferedReader reader = new BufferedReader(new FileReader(filePath))) {
            String line;
            while ((line = reader.readLine()) != null) {
                if ((line.contains("=")) && (!(line.contains("#")))) {
                    String[] parts = line.split("=", 2);
                    String variable = parts[0].trim();
                    String defaultValue = (parts.length > 1) ? parts[1].trim() : "null";
                    variables.put(variable, defaultValue);
                }
            }
        } catch (IOException e) {
            logger.info("Ошибка при чтении файла " + filePath + ": " + e.getMessage());
        }
    }

    /**
     * Метод, который заменяет ссылки на переменные в значении каждой пары ключ-значение в заданной карте переменных.
     *
     * @param variables исходная карта переменных
     * @return новая карта переменных с замененными ссылками и преобразованными ключами
     */
    public static Map<String, String> replaceVariableReferences(Map<String, String> variables) {
        Map<String, String> newVariables = new HashMap<>();

        for (Map.Entry<String, String> entry : variables.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            String modifiedValue = replaceVariables(value, variables);
            if ((!isValidStyle(key)) && (!(modifiedValue.contains("$")))) {
                newVariables.put(key, modifiedValue);
            }
        }
        return newVariables;
    }

    /**
     * Метод, который проверяет, соответствует ли заданная переменная стилю Camel Case.
     *
     * @param key переменная для проверки стиля
     * @return true, если строка соответствует стилю Camel Case, иначе false.
     */
    private static boolean isValidStyle(String key) {
        return key.matches("^[a-z]+([A-Z][a-z0-9]+)*$");
    }

    public static String replaceVariables(String value, Map<String, String> variables) {
        Pattern pattern = Pattern.compile("\\$(\\w+)");
        Matcher matcher = pattern.matcher(value);

        if (matcher.find()) {
            String variableKey = matcher.group(1);
            if (variables.containsKey(variableKey)) {
                String variableValue = variables.get(variableKey);
                String modifiedValue = value.replace("$" + variableKey, variableValue);
                return replaceVariables(modifiedValue, variables);
            }
        }
        return value;
    }

    /**
     * Метод, который генерирует форматированный вывод переменных и их значений в формате XML.
     *
     * @param variables карта с переменными, где ключ - имя переменной, а значение - ее значение.
     */
    public static void printXml(Map<String, String> variables) {
        StringBuilder xml = new StringBuilder("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n");
        xml.append("<envs>\n");

        for (Map.Entry<String, String> entry : variables.entrySet()) {

            xml.append(String.format("  <env name=\"%s\" value=\"%s\"/>%n", entry.getKey(), entry.getValue()));
        }

        xml.append("</envs>");

        logger.info(xml.toString());
    }
}

