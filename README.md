# Библиотека Parser


## Описание библиотеки

Подключенная библиотека читает файлы application.yml, build.gradle и gradle.properties.

#### Результат:

Печатает получившиеся переменные и их значения в формате xml.
Пример (с нужными отступами):

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<envs>
<env name="ARANGO_TIMEOUT" value="25000"/>
<env name="KAFKA_HOSTS" value="localhost:9092"/>
   ...
<env name="VALIDATION_ENABLED" value="true"/>
</envs>
```

## Описание API:

1. Реализован метод readAppFile, который читает файл application.yml и выделяет из него переменные с дефолтными значениеми, если есть.\
   **Пример переменной:** `${ARANGO_DB}`\
   **Пример переменной с дефолтным значением:** `${ARANGO_HEALTH_CLUSTER:false}`\
   **Пример переменной с пустым дефолтным значением:** `${ARANGO_USER:}`\
   Переменная в тексте файла может находиться где угодно, а также может быть несколько переменных в одной строке. Пример из файла:
```yaml
   timeout: ${ARANGO_TIMEOUT:25000} # in millis
   protocol: VST
   max-connections: ${ARANGO_MAX_POOL_SIZE:120}
   ```
3. Реализован метод readGradleFile, который читает файл build.gradle и извлекает переменные и их значения.
4. Реализован метод readPropertiesFile, который читает файл gradle.properties и извлекает переменные и их значения.
5. Реализован метод replaceVariableReferences, который заменяет ссылки на переменные в значении каждой пары ключ-значение в заданной карте переменных.\
   Из build.gradle файла достаются переменные с значениями ссылками из блока run:
```yaml 
run {
   environment([
   "TRUSTED_ISSUERS"            : "$trustedIssuers",
   "ARANGO_HEALTH_CLUSTER"      : "$arangoHealthCluster",
   "ARANGO_HOST"                : "$arangoHost",
   "ARANGO_PORT"                : "$arangoPort",
   "ARANGO_DB"                  : "$arangoDb",
   "ARANGO_USER"                : "$arangoUser",
   "ARANGO_PASS"                : "$arangoPassword",
   "KAFKA_HOSTS"                : "$kafkaHost:$kafkaPort",
   "JAEGER_ENABLED"             : "$jaegerEnabled",
   "JAEGER_HOST"                : "$jaegerHost",
   "JAEGER_PORT"                : "$jaegerPort",
   "SECURITY_ENABLED"           : false,
   "VALIDATION_ENABLED"         : true,
   "STORAGE_TRANSACTION_ENABLED": "$enableTransaction"
   ])
   }
```
Если значение переменной содержит ссылку (или ссылки) на другую переменную, то вместо ссылки нужно подставить её значение из gradle.properties файла.
Т.е. имеем\
`"KAFKA_HOSTS": "$kafkaHost:$kafkaPort"`\
в gradle.properties файле
```yaml 
   kafkaHost=localhost
   kafkaPort=9092
   в результате значение переменной
   KAFKA_HOSTS=localhost:9092
```
6. Реализован метод isValidStyle, который проверяет, соответствует ли заданная переменная стилю Camel Case.

7. Реализован метод printXml, который генерирует форматированный вывод переменных и их значений в формате XML.

## Подключение библиотеки в проект

* В файле pom.xml в блок `<dependencies>` добавить следующий код:
```xml
<dependency>
   <groupId>com.vpavlova</groupId>
   <artifactId>parser</artifactId>
   <version>1.0-SNAPSHOT</version>
</dependency>
```
## Разработчик

* **NAME**: Павлова Виктория

* **E-MAIL**: viktoriya.n.pavlova@rt.ru